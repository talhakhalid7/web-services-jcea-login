﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;


namespace E_xception
{
    [WebService(Namespace = "http://jcea.org.sa")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
 
    public class WebService1 : System.Web.Services.WebService
    {
        string password;
        private bool IsExist;

        //public object ds { get; private set; }

        [WebMethod]
        public string login(string username, string pass)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbconnection"].ToString());
            con.Open();
            SqlCommand cdp = new SqlCommand("select * from Users where username='" + username + "'", con);
            SqlDataReader sdr = cdp.ExecuteReader();
            if (sdr.Read())
            {
                password = sdr.GetString(6);  //get the user password from db if the user name is exist in that.  
                IsExist = true;
                sdr.Close();
            }
           
            if (IsExist)  //if record exis in db , it will return true, otherwise it will return false  
            {
                password = Hashing.Decrypt(password).ToLower();

                if (password==pass)
                {
                    cdp.ExecuteNonQuery();
                    SqlDataAdapter da = new SqlDataAdapter(cdp);
                    // Create an instance of DataSet.
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    con.Close();
                    string json =JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.Indented);
                    return json ; 
                }

                
            }
           
            
            return null;
             
        }

        private DataSet DataTableToJSONWithJSONNet()
        {
            throw new NotImplementedException();
        }

        
    }

    

}

