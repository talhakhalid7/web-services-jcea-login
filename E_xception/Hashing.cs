﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace E_xception
{
    public class Hashing
    {
        private const string Password = "Password";
        private const string Salt = "salt";
        public Hashing() { }

        //public static string Encrypt(string input)
        //{
        //    #region Code Area
        //    using (SHA1CryptoServiceProvider sha = new SHA1CryptoServiceProvider())
        //    {
        //        //return Encoding.Default.GetString(sha.ComputeHash(Encoding.Default.GetBytes(input)));
        //        byte[] data = sha.ComputeHash(Encoding.Default.GetBytes(input));
        //        StringBuilder sBuilder = new StringBuilder();

        //        for (int i = 0; i < data.Length; i++)
        //            sBuilder.Append(data[i].ToString("X2"));

        //        return sBuilder.ToString();
        //    }
        //    #endregion
        //}

        public static string Encrypt(string Text)
        {
            return SymmEncrypt<AesManaged>(Text);
        }

        public static string Decrypt(string Text)
        {
            return SymmDecrypt<AesManaged>(Text);
        }

        private static string SymmEncrypt<T>(string input) where T : SymmetricAlgorithm, new()
        {
            DeriveBytes db = new Rfc2898DeriveBytes(Password, Encoding.Unicode.GetBytes(Salt));
            SymmetricAlgorithm sym = new T();
            byte[] dbkey = db.GetBytes(sym.KeySize >> 3);
            byte[] dbIV = db.GetBytes(sym.BlockSize >> 3);

            ICryptoTransform ct = sym.CreateEncryptor(dbkey, dbIV);
            int mod = input.Length % 4 ;
            if (mod> 0)
            {
                input += new string('=', 4 - mod);
            }
            input.Replace(' ', '+');

            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, ct, CryptoStreamMode.Write))
                {
                    using (StreamWriter sr = new StreamWriter(cs, Encoding.Unicode))
                    {
                        sr.Write(input);
                    }
                    byte[] Encrypted = ms.ToArray();
                    return Convert.ToBase64String(Encrypted);
                }
            }
        }

        private static string SymmDecrypt<T>(string text)
   where T : SymmetricAlgorithm, new()
        {
            DeriveBytes rgb = new Rfc2898DeriveBytes(Password, Encoding.Unicode.GetBytes(Salt));

            SymmetricAlgorithm algorithm = new T();

            byte[] rgbKey = rgb.GetBytes(algorithm.KeySize >> 3);
            byte[] rgbIV = rgb.GetBytes(algorithm.BlockSize >> 3);

            ICryptoTransform transform = algorithm.CreateDecryptor(rgbKey, rgbIV);
            int mod = text.Length % 4;
            if (mod > 0)
            {
                text += new string('=', 4 - mod);
            }
            text.Replace(' ', '+');
            using (MemoryStream buffer = new MemoryStream(Convert.FromBase64String(text)))
            {
                using (CryptoStream stream = new CryptoStream(buffer, transform, CryptoStreamMode.Read))
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.Unicode))
                    {
                        string Decrypted = reader.ReadToEnd();
                        Decrypted = Decrypted.TrimEnd('=');
                        return Decrypted;
                    }
                }
            }
        }
    }
}
